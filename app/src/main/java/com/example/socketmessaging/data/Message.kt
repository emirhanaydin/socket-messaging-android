package com.example.socketmessaging.data

interface Message {
    val body: String
    val createdAt: Long
}