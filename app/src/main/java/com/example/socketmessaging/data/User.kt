package com.example.socketmessaging.data

data class User(val email: String, val name: String)