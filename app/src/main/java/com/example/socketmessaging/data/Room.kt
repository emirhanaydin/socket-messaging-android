package com.example.socketmessaging.data

data class Room(val name: String, val iconName: String? = null)