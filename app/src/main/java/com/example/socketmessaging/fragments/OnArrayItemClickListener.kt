package com.example.socketmessaging.fragments

interface OnArrayItemClickListener {
    fun onArrayItemClick(index: Int)
}
