package com.example.socketmessaging.adapters

import android.view.View

interface OnItemClickListener {
    fun onItemClick(position: Int, view: View)
}