package com.example.socketmessaging

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_signup.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException

class SignUpActivity : AppCompatActivity() {
    companion object {
        const val TAG = "SignUpActivity"
        private const val ERR_RES_EMAIL_IN_USE = "is already in use"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        buttonSignUp.setOnClickListener { signUp() }
        linkLogin.setOnClickListener {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out)
        }
    }

    private fun signUp() {
        Log.d(TAG, "Sign Up")

        if (!validateInput()) {
            onSignUpFailed()
            return
        }

        buttonSignUp.isEnabled = false

        val progressDialog = ProgressDialog(this, R.style.AppTheme_Dark_Dialog)
        progressDialog.apply {
            isIndeterminate = true
            setMessage("Signing up...")
            show()
        }

        fun getInputStr(editText: EditText): String {
            return editText.text.toString()
        }

        val name = getInputStr(inputName)
        val email = getInputStr(inputEmail)
        val password = getInputStr(inputPassword)

        val url = "${Connection.URL}/api/users"

        val jsonUser = JSONObject()
        jsonUser.put("name", name)
        jsonUser.put("email", email)
        jsonUser.put("password", password)
        val jsonBody = JSONObject()
        jsonBody.put("user", jsonUser)

        val responseListener = Response.Listener<JSONObject> {
            progressDialog.dismiss()

            onSignUpSuccess(it.getJSONObject("user"))
        }
        val errorListener = Response.ErrorListener {
            var errorMessage = "An error occurred while registering"

            it.networkResponse?.apply {
                data?.apply {
                    try {
                        val bodyStr = String(this, Charsets.UTF_8)
                        val body = JSONObject(bodyStr)

                        val errors = body.getJSONObject("errors")

                        errors.takeIf { e -> e.has("email") }?.apply {
                            when (getString("email")) {
                                ERR_RES_EMAIL_IN_USE -> inputEmail.error = "This email is already in use"
                            }

                            inputEmail.requestFocus()
                            errorMessage = ""
                        }
                    } catch (e: UnsupportedEncodingException) {
                        e.printStackTrace()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            } ?: run { errorMessage = "Server not responding" }

            onSignUpFailed(errorMessage)

            progressDialog.dismiss()
        }
        val registerUserRequest = JsonObjectRequest(Request.Method.POST, url, jsonBody, responseListener, errorListener)

        val queue = Volley.newRequestQueue(this)
        queue.add(registerUserRequest)
    }

    private fun onSignUpSuccess(userJson: JSONObject) {
        buttonSignUp.isEnabled = true
        setResult(Activity.RESULT_OK)

        val intent = Intent().putExtra(LoginActivity.EXTRA_USER, userJson.toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun onSignUpFailed(errorMessage: String = "") {
        if (errorMessage.isNotBlank())
            Toast.makeText(baseContext, errorMessage, Toast.LENGTH_LONG).show()

        buttonSignUp.isEnabled = true
    }

    private fun validateInput(): Boolean {
        val editTexts = arrayOf(
            inputName,
            inputEmail,
            inputPassword,
            inputRePassword
        )

        val inputs = editTexts.map { i -> i.text.toString() }

        val conditions = arrayOf(
            inputs[0].run { isNotBlank() || length >= 3 },
            inputs[1].let { it.isNotBlank() || android.util.Patterns.EMAIL_ADDRESS.matcher(it).matches() },
            inputs[2].run { isNotBlank() || length >= 4 },
            inputs[3] == inputs[2]
        )

        val errorMessages = arrayOf(
            "Name must be at least 3 characters long",
            "enter a valid email address",
            "Password must be at least 4 characters long",
            "Passwords do not match"
        )

        var valid = true

        for (i: Int in 0 until inputs.size) {
            if (valid && !conditions[i]) valid = false

            editTexts[i].error = if (conditions[i]) null else errorMessages[i]
        }

        return valid
    }
}
